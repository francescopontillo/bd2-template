package net.frapontillo.uni.bd2.template.filter;

import javax.ws.rs.ext.Provider;

import net.frapontillo.uni.bd2.template.exception.NotAuthorizedException;

import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;
import com.sun.jersey.spi.container.ContainerResponseFilter;
import com.sun.jersey.spi.container.ResourceFilter;

@Provider
public class AuthenticationFilter implements ResourceFilter, ContainerRequestFilter {

	@Override
	public ContainerRequest filter(ContainerRequest req) {
		String authcode = req.getQueryParameters().getFirst("authcode");
		// TODO: gestire codice lato DB
		if (authcode == null || authcode == "") throw new NotAuthorizedException();
		return req;
	}

	@Override
	public ContainerRequestFilter getRequestFilter() {
		return this;
	}

	@Override
	public ContainerResponseFilter getResponseFilter() {
		return null;
	}

}
