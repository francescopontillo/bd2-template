package net.frapontillo.uni.bd2.template.filter;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.ext.Provider;

import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerResponse;
import com.sun.jersey.spi.container.ContainerResponseFilter;

@Provider
public class CorsResponseFilter implements ContainerResponseFilter {
	@Override
	public ContainerResponse filter(ContainerRequest req, ContainerResponse res) {
		ResponseBuilder resp = Response.fromResponse(res.getResponse());
        resp.header("Access-Control-Allow-Origin", "*")
        	.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
 
        String reqHead = req.getHeaderValue("Access-Control-Request-Headers");
 
        if (null != reqHead && !reqHead.equals(null)) {
            resp.header("Access-Control-Allow-Headers", reqHead);
        }

        res.setResponse(resp.build());
        return res;
	}
}
