package net.frapontillo.uni.bd2.template.filter;

import java.util.NoSuchElementException;

import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;

import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerResponse;
import com.sun.jersey.spi.container.ContainerResponseFilter;

@Provider
public class NullResponseFilter implements ContainerResponseFilter {
	@Override
	public ContainerResponse filter(ContainerRequest req, ContainerResponse res) {
		if (res.getEntity() == null && !req.getMethod().equals("OPTIONS") && res.getStatus() == Status.OK.getStatusCode())
			throw new NoSuchElementException("Element not found.");
		if (req.getMethod().equals("OPTIONS")) {
			res.setStatus(200);
		}
		return res;
	}
}
