package net.frapontillo.uni.bd2.template.resource;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import net.frapontillo.uni.bd2.template.converter.PartecipazioneConverter;
import net.frapontillo.uni.bd2.template.db.IngegnereDB;
import net.frapontillo.uni.bd2.template.db.PartecipazioneDB;
import net.frapontillo.uni.bd2.template.entity.Partecipazione;
import net.frapontillo.uni.bd2.template.filter.AuthenticationFilter;
import net.frapontillo.uni.bd2.template.util.DBUtil;

import org.apache.cayenne.exp.ExpressionFactory;
import org.apache.cayenne.query.SelectQuery;

import com.sun.jersey.spi.container.ResourceFilters;

@Path("partecipazione/")
@ResourceFilters(AuthenticationFilter.class)
public class PartecipazioneResource {
	@GET
	@Path("{id}")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Partecipazione getPartecipazione(@PathParam("id") String id) {
		int mId = Integer.parseInt(id);
		SelectQuery query = new SelectQuery(PartecipazioneDB.class,
				ExpressionFactory.matchDbExp(PartecipazioneDB.ID_PK_COLUMN, mId));
		List<PartecipazioneDB> partecipaziones = (List<PartecipazioneDB>)DBUtil.getContext().performQuery(query);
		PartecipazioneDB dbPartecipazione = partecipaziones.size() > 0 ? (PartecipazioneDB)partecipaziones.get(0) : null;
		Partecipazione partecipazione = new PartecipazioneConverter().from(dbPartecipazione);
		return partecipazione;
	}
	
	@POST
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Partecipazione postPartecipazione(Partecipazione partecipazione) {
		return partecipazione;
	}
	
	@PUT
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Partecipazione putPartecipazione(Partecipazione partecipazione) {
		return partecipazione;
	}
	
	@DELETE
	public void deletePartecipazione(String id) {
		return;
	}
}
