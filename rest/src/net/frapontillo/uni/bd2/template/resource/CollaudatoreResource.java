package net.frapontillo.uni.bd2.template.resource;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import net.frapontillo.uni.bd2.template.converter.CollaudatoreConverter;
import net.frapontillo.uni.bd2.template.db.AutoDB;
import net.frapontillo.uni.bd2.template.db.CollaudatoreDB;
import net.frapontillo.uni.bd2.template.entity.Collaudatore;
import net.frapontillo.uni.bd2.template.filter.AuthenticationFilter;
import net.frapontillo.uni.bd2.template.util.DBUtil;

import org.apache.cayenne.exp.ExpressionFactory;
import org.apache.cayenne.query.SelectQuery;

import com.sun.jersey.spi.container.ResourceFilters;

@Path("collaudatore/")
@ResourceFilters(AuthenticationFilter.class)
public class CollaudatoreResource {
	@GET
	@Path("{id}")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Collaudatore getCollaudatore(@PathParam("id") String id) {
		int mId = Integer.parseInt(id);
		SelectQuery query = new SelectQuery(CollaudatoreDB.class,
				ExpressionFactory.matchDbExp(CollaudatoreDB.ID_PK_COLUMN, mId));
		List<CollaudatoreDB> collaudatores = (List<CollaudatoreDB>)DBUtil.getContext().performQuery(query);
		CollaudatoreDB dbCollaudatore =  collaudatores.size() > 0 ? (CollaudatoreDB)collaudatores.get(0) : null;
		Collaudatore collaudatore = new CollaudatoreConverter().from(dbCollaudatore);
		return collaudatore;
	}
	
	@POST
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Collaudatore postCollaudatore(Collaudatore collaudatore) {
		return collaudatore;
	}
	
	@PUT
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Collaudatore putCollaudatore(Collaudatore collaudatore) {
		return collaudatore;
	}
	
	@DELETE
	public void deleteCollaudatore(String id) {
		return;
	}
}
