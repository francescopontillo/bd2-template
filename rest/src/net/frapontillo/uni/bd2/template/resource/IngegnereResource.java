package net.frapontillo.uni.bd2.template.resource;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import net.frapontillo.uni.bd2.template.converter.IngegnereConverter;
import net.frapontillo.uni.bd2.template.db.GaraDB;
import net.frapontillo.uni.bd2.template.db.IngegnereDB;
import net.frapontillo.uni.bd2.template.entity.Ingegnere;
import net.frapontillo.uni.bd2.template.filter.AuthenticationFilter;
import net.frapontillo.uni.bd2.template.util.DBUtil;

import org.apache.cayenne.exp.ExpressionFactory;
import org.apache.cayenne.query.SelectQuery;

import com.sun.jersey.spi.container.ResourceFilters;

@Path("ingegnere/")
@ResourceFilters(AuthenticationFilter.class)
public class IngegnereResource {
	@GET
	@Path("{id}")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Ingegnere getIngegnere(@PathParam("id") String id) {
		int mId = Integer.parseInt(id);
		SelectQuery query = new SelectQuery(IngegnereDB.class,
				ExpressionFactory.matchDbExp(IngegnereDB.ID_PK_COLUMN, mId));
		List<IngegnereDB> ingegneres = (List<IngegnereDB>)DBUtil.getContext().performQuery(query);
		IngegnereDB dbIngegnere = ingegneres.size() > 0 ? (IngegnereDB)ingegneres.get(0) : null;
		Ingegnere ingegnere = new IngegnereConverter().from(dbIngegnere);
		return ingegnere;
	}
	
	@POST
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Ingegnere postIngegnere(Ingegnere ingegnere) {
		return ingegnere;
	}
	
	@PUT
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Ingegnere putIngegnere(Ingegnere ingegnere) {
		return ingegnere;
	}
	
	@DELETE
	public void deleteIngegnere(String id) {
		return;
	}
}
