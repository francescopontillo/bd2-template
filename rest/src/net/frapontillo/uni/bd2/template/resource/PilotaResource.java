package net.frapontillo.uni.bd2.template.resource;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import net.frapontillo.uni.bd2.template.converter.PilotaConverter;
import net.frapontillo.uni.bd2.template.db.PartecipazioneDB;
import net.frapontillo.uni.bd2.template.db.PilotaDB;
import net.frapontillo.uni.bd2.template.entity.Pilota;
import net.frapontillo.uni.bd2.template.filter.AuthenticationFilter;
import net.frapontillo.uni.bd2.template.util.DBUtil;

import org.apache.cayenne.exp.ExpressionFactory;
import org.apache.cayenne.query.SelectQuery;

import com.sun.jersey.spi.container.ResourceFilters;

@Path("pilota/")
@ResourceFilters(AuthenticationFilter.class)
public class PilotaResource {
	@GET
	@Path("{id}")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Pilota getPilota(@PathParam("id") String id) {
		int mId = Integer.parseInt(id);
		SelectQuery query = new SelectQuery(PilotaDB.class,
				ExpressionFactory.matchDbExp(PilotaDB.ID_PK_COLUMN, mId));
		List<PilotaDB> pilotas = (List<PilotaDB>)DBUtil.getContext().performQuery(query);
		PilotaDB dbPilota = pilotas.size() > 0 ? (PilotaDB)pilotas.get(0) : null;
		Pilota pilota = new PilotaConverter().from(dbPilota);
		return pilota;
	}
	
	@POST
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Pilota postPilota(Pilota pilota) {
		return pilota;
	}
	
	@PUT
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Pilota putPilota(Pilota pilota) {
		return pilota;
	}
	
	@DELETE
	public void deletePilota(String id) {
		return;
	}
}
