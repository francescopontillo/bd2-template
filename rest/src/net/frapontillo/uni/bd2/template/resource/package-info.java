/**
 * Package contenente tutti i Controller delle Entità,
 * Fungono da endpoint per una risorsa.
 * @author Francesco Pontillo
 *
 */
package net.frapontillo.uni.bd2.template.resource;