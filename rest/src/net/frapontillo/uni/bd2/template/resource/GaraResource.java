package net.frapontillo.uni.bd2.template.resource;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.cayenne.exp.ExpressionFactory;
import org.apache.cayenne.query.SelectQuery;

import com.sun.jersey.spi.container.ResourceFilters;

import net.frapontillo.uni.bd2.template.converter.GaraConverter;
import net.frapontillo.uni.bd2.template.db.CollaudatoreDB;
import net.frapontillo.uni.bd2.template.db.GaraDB;
import net.frapontillo.uni.bd2.template.entity.Gara;
import net.frapontillo.uni.bd2.template.filter.AuthenticationFilter;
import net.frapontillo.uni.bd2.template.util.DBUtil;

@Path("gara/")
@ResourceFilters(AuthenticationFilter.class)
public class GaraResource {
	@GET
	@Path("{id}")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Gara getGara(@PathParam("id") String id) {
		int mId = Integer.parseInt(id);
		SelectQuery query = new SelectQuery(GaraDB.class,
				ExpressionFactory.matchDbExp(GaraDB.ID_PK_COLUMN, mId));
		List<GaraDB> garas = (List<GaraDB>)DBUtil.getContext().performQuery(query);
		GaraDB dbGara = garas.size() > 0 ? (GaraDB)garas.get(0) : null;
		Gara gara = new GaraConverter().from(dbGara);
		return gara;
	}
	
	@POST
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Gara postGara(Gara gara) {
		return gara;
	}
	
	@PUT
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Gara putGara(Gara gara) {
		return gara;
	}
	
	@DELETE
	public void deleteGara(String id) {
		return;
	}
}
