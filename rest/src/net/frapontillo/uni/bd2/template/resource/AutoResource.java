package net.frapontillo.uni.bd2.template.resource;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import net.frapontillo.uni.bd2.template.converter.AutoConverter;
import net.frapontillo.uni.bd2.template.db.AutoDB;
import net.frapontillo.uni.bd2.template.entity.Auto;
import net.frapontillo.uni.bd2.template.filter.AuthenticationFilter;
import net.frapontillo.uni.bd2.template.util.DBUtil;

import org.apache.cayenne.exp.ExpressionFactory;
import org.apache.cayenne.query.SelectQuery;

import com.sun.jersey.spi.container.ResourceFilters;

@Path("auto/")
@ResourceFilters(AuthenticationFilter.class)
public class AutoResource {
	@GET
	@Path("{id}")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Auto getAuto(@PathParam("id") String id) {
		SelectQuery query = new SelectQuery(AutoDB.class,
				ExpressionFactory.matchDbExp(AutoDB.SIGLA_PK_COLUMN, id));
		List<AutoDB> autos = (List<AutoDB>)DBUtil.getContext().performQuery(query);
		AutoDB dbAuto = autos.size() > 0 ? (AutoDB)autos.get(0) : null;
		Auto auto = new AutoConverter().from(dbAuto);
		return auto;
	}
	
	@POST
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Auto postAuto(Auto auto) {
		return auto;
	}
	
	@PUT
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Auto putAuto(Auto auto) {
		return auto;
	}
	
	@DELETE
	public void deleteAuto(String id) {
		return;
	}
}
