package net.frapontillo.uni.bd2.template.entity;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Collaudatore extends Membro implements IEntity {
	protected Auto autoCollaudo;

	public Collaudatore() {
		super();
	}
	
	/**
	 * @param iD
	 * @param nome
	 * @param cognome
	 * @param dataNascita
	 * @param luogoNascita
	 * @param autoCollaudo
	 */
	public Collaudatore(long iD, String nome, String cognome, Date dataNascita,
			String luogoNascita, Auto autoCollaudo) {
		super(iD, nome, cognome, dataNascita, luogoNascita);
		this.autoCollaudo = autoCollaudo;
	}

	/**
	 * @return the autoCollaudo
	 */
	public Auto getAutoCollaudo() {
		return autoCollaudo;
	}

	/**
	 * @param autoCollaudo the autoCollaudo to set
	 */
	public void setAutoCollaudo(Auto autoCollaudo) {
		this.autoCollaudo = autoCollaudo;
	}
	
	
}
