package net.frapontillo.uni.bd2.template.entity;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Ingegnere extends Membro implements IEntity {
	protected String specializzazione;

	public Ingegnere() {
		super();
	}
	
	/**
	 * @param iD
	 * @param nome
	 * @param cognome
	 * @param dataNascita
	 * @param luogoNascita
	 * @param specializzazione
	 */
	public Ingegnere(long iD, String nome, String cognome, Date dataNascita,
			String luogoNascita, String specializzazione) {
		super(iD, nome, cognome, dataNascita, luogoNascita);
		this.specializzazione = specializzazione;
	}

	/**
	 * @return the specializzazione
	 */
	public String getSpecializzazione() {
		return specializzazione;
	}

	/**
	 * @param specializzazione the specializzazione to set
	 */
	public void setSpecializzazione(String specializzazione) {
		this.specializzazione = specializzazione;
	}
}
