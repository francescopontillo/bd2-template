package net.frapontillo.uni.bd2.template.entity;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Partecipazione implements IEntity {
	protected Pilota pilota;
	protected Auto auto;
	protected Gara gara;
	protected Boolean vittoria;
	protected Boolean giroVeloce;

	public Partecipazione() {
		super();
	}
	
	/**
	 * @param pilota
	 * @param auto
	 * @param gara
	 * @param vittoria
	 * @param giroVeloce
	 */
	public Partecipazione(Pilota pilota, Auto auto, Gara gara,
			Boolean vittoria, Boolean giroVeloce) {
		super();
		this.pilota = pilota;
		this.auto = auto;
		this.gara = gara;
		this.vittoria = vittoria;
		this.giroVeloce = giroVeloce;
	}
	
	/**
	 * @return the pilota
	 */
	public Pilota getPilota() {
		return pilota;
	}
	/**
	 * @param pilota the pilota to set
	 */
	public void setPilota(Pilota pilota) {
		this.pilota = pilota;
	}
	/**
	 * @return the auto
	 */
	public Auto getAuto() {
		return auto;
	}
	/**
	 * @param auto the auto to set
	 */
	public void setAuto(Auto auto) {
		this.auto = auto;
	}
	/**
	 * @return the gara
	 */
	public Gara getGara() {
		return gara;
	}
	/**
	 * @param gara the gara to set
	 */
	public void setGara(Gara gara) {
		this.gara = gara;
	}
	/**
	 * @return the vittoria
	 */
	public Boolean isVittoria() {
		return vittoria;
	}
	/**
	 * @param vittoria the vittoria to set
	 */
	public void setVittoria(Boolean vittoria) {
		this.vittoria = vittoria;
	}
	/**
	 * @return the giroVeloce
	 */
	public Boolean isGiroVeloce() {
		return giroVeloce;
	}
	/**
	 * @param giroVeloce the giroVeloce to set
	 */
	public void setGiroVeloce(Boolean giroVeloce) {
		this.giroVeloce = giroVeloce;
	}
}
