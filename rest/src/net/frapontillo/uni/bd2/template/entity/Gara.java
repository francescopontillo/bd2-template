package net.frapontillo.uni.bd2.template.entity;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Gara implements IEntity {
	protected int ID;
	protected String nome;
	protected String luogo;
	protected Date data;
	protected Integer giroVeloce;
	
	public Gara() {
		super();
	}
	
	/**
	 * @param iD
	 * @param nome
	 * @param luogo
	 * @param data
	 * @param giroVeloce
	 */
	public Gara(int ID, String nome, String luogo, Date data, Integer giroVeloce) {
		super();
		this.ID = ID;
		this.nome = nome;
		this.luogo = luogo;
		this.data = data;
		this.giroVeloce = giroVeloce;
	}
	
	/**
	 * @return the iD
	 */
	public int getID() {
		return ID;
	}
	/**
	 * @param iD the iD to set
	 */
	public void setID(int iD) {
		ID = iD;
	}
	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}
	/**
	 * @param nome the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	/**
	 * @return the luogo
	 */
	public String getLuogo() {
		return luogo;
	}
	/**
	 * @param luogo the luogo to set
	 */
	public void setLuogo(String luogo) {
		this.luogo = luogo;
	}
	/**
	 * @return the data
	 */
	public Date getData() {
		return data;
	}
	/**
	 * @param data the data to set
	 */
	public void setData(Date data) {
		this.data = data;
	}
	/**
	 * @return the giroVeloce
	 */
	public Integer getGiroVeloce() {
		return giroVeloce;
	}
	/**
	 * @param giroVeloce the giroVeloce to set
	 */
	public void setGiroVeloce(Integer giroVeloce) {
		this.giroVeloce = giroVeloce;
	}
}
