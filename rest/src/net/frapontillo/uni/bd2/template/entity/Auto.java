package net.frapontillo.uni.bd2.template.entity;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Auto implements IEntity {
	protected String sigla;
	protected String nome;
	protected Long potenza;
	protected String tipoMotore;
	protected String descrizioneTecnica;
	protected List<Collaudatore> collaudatori;
	
	public Auto() {
		super();
	}

	/**
	 * @param sigla
	 * @param nome
	 * @param potenza
	 * @param tipoMotore
	 * @param descrizioneTecnica
	 */
	public Auto(String sigla, String nome, Long potenza, String tipoMotore,
			String descrizioneTecnica) {
		super();
		this.sigla = sigla;
		this.nome = nome;
		this.potenza = potenza;
		this.tipoMotore = tipoMotore;
		this.descrizioneTecnica = descrizioneTecnica;
	}

	/**
	 * @return the sigla
	 */
	public String getSigla() {
		return sigla;
	}

	/**
	 * @param sigla the sigla to set
	 */
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * @return the potenza
	 */
	public Long getPotenza() {
		return potenza;
	}

	/**
	 * @param potenza the potenza to set
	 */
	public void setPotenza(Long potenza) {
		this.potenza = potenza;
	}

	/**
	 * @return the tipoMotore
	 */
	public String getTipoMotore() {
		return tipoMotore;
	}

	/**
	 * @param tipoMotore the tipoMotore to set
	 */
	public void setTipoMotore(String tipoMotore) {
		this.tipoMotore = tipoMotore;
	}

	/**
	 * @return the descrizioneTecnica
	 */
	public String getDescrizioneTecnica() {
		return descrizioneTecnica;
	}

	/**
	 * @param descrizioneTecnica the descrizioneTecnica to set
	 */
	public void setDescrizioneTecnica(String descrizioneTecnica) {
		this.descrizioneTecnica = descrizioneTecnica;
	}

	/**
	 * @return the collaudatori
	 */
	public List<Collaudatore> getCollaudatori() {
		return collaudatori;
	}

	/**
	 * @param collaudatori the collaudatori to set
	 */
	public void setCollaudatori(List<Collaudatore> collaudatori) {
		this.collaudatori = collaudatori;
	}

}
