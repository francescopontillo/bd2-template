/**
 * Package contenente tutte le entità da esporre.
 * @author Francesco Pontillo
 *
 */
package net.frapontillo.uni.bd2.template.entity;