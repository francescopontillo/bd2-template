package net.frapontillo.uni.bd2.template.entity;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Pilota extends Membro implements IEntity {
	protected int vittorie;
	protected int campionati;

	public Pilota() {
		super();
	}
	
	/**
	 * @param iD
	 * @param nome
	 * @param cognome
	 * @param dataNascita
	 * @param luogoNascita
	 * @param vittorie
	 * @param campionati
	 */
	public Pilota(long iD, String nome, String cognome, Date dataNascita,
			String luogoNascita, int vittorie, int campionati) {
		super(iD, nome, cognome, dataNascita, luogoNascita);
		this.vittorie = vittorie;
		this.campionati = campionati;
	}
	
	/**
	 * @return the vittorie
	 */
	public int getVittorie() {
		return vittorie;
	}
	/**
	 * @param vittorie the vittorie to set
	 */
	public void setVittorie(int vittorie) {
		this.vittorie = vittorie;
	}
	/**
	 * @return the campionati
	 */
	public int getCampionati() {
		return campionati;
	}
	/**
	 * @param campionati the campionati to set
	 */
	public void setCampionati(int campionati) {
		this.campionati = campionati;
	}
}
