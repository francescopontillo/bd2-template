package net.frapontillo.uni.bd2.template.converter;

import net.frapontillo.uni.bd2.template.db.GaraDB;
import net.frapontillo.uni.bd2.template.entity.Gara;
import net.frapontillo.uni.bd2.template.util.DBUtil;

public class GaraConverter extends AbstractConverter<GaraDB, Gara> {
	@Override
	public Gara from(GaraDB source, int lev) {
		if (source == null) return null;
		Gara gara = new Gara();
		if (lev >= CONV_TYPE.MINIMUM) {
			gara.setID((Integer) DBUtil.getID(source, "ID"));
			gara.setNome(source.getNome());
			gara.setData(source.getData());
			gara.setLuogo(source.getLuogo());
		}
		if (lev >= CONV_TYPE.NORMAL) {
			gara.setGiroVeloce(source.getGiroVeloce());
		}
		if (lev >= CONV_TYPE.CASCADE) {
		}
		return gara;
	}

	@Override
	public GaraDB to(Gara source, int lev) {
		GaraDB gara = new GaraDB();
		if (lev >= CONV_TYPE.MINIMUM) {
			gara.setData(source.getData());
			gara.setLuogo(source.getLuogo());
			gara.setNome(source.getNome());
		}
		if (lev >= CONV_TYPE.NORMAL) {
			gara.setGiroVeloce(source.getGiroVeloce());
		}
		if (lev >= CONV_TYPE.CASCADE) {
		}
		return gara;
	}
}
