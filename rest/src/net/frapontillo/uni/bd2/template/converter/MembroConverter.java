package net.frapontillo.uni.bd2.template.converter;

import net.frapontillo.uni.bd2.template.db.MembroDB;
import net.frapontillo.uni.bd2.template.entity.Membro;
import net.frapontillo.uni.bd2.template.util.DBUtil;

public class MembroConverter extends AbstractConverter<MembroDB, Membro> {
	@Override
	public Membro from(MembroDB source, int lev) {
		if (source == null) return null;
		Membro membro = new Membro();
		if (lev >= CONV_TYPE.MINIMUM) {
			membro.setID((Long) DBUtil.getID(source, "ID"));
			membro.setCognome(source.getCognome());
			membro.setNome(source.getNome());
		}
		if (lev >= CONV_TYPE.NORMAL) {
			membro.setDataNascita(source.getDataNascita());
			membro.setLuogoNascita(source.getLuogoNascita());
		}
		if (lev >= CONV_TYPE.CASCADE) {
		}
		return membro;
	}

	@Override
	public MembroDB to(Membro source, int lev) {
		MembroDB membro = new MembroDB();
		if (lev >= CONV_TYPE.MINIMUM) {
			membro.setCognome(source.getCognome());
			membro.setNome(source.getNome());
		}
		if (lev >= CONV_TYPE.NORMAL) {
			membro.setDataNascita(source.getDataNascita());
			membro.setLuogoNascita(source.getLuogoNascita());
		}
		if (lev >= CONV_TYPE.CASCADE) {
		}
		return membro;
	}
}
