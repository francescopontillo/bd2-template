package net.frapontillo.uni.bd2.template.converter;

import net.frapontillo.uni.bd2.template.db.PartecipazioneDB;
import net.frapontillo.uni.bd2.template.entity.Partecipazione;

public class PartecipazioneConverter extends AbstractConverter<PartecipazioneDB, Partecipazione> {

	@Override
	public Partecipazione from(PartecipazioneDB source, int lev) {
		if (source == null) return null;
		Partecipazione partecipazione = new Partecipazione();
		if (lev >= CONV_TYPE.MINIMUM) {
			partecipazione.setAuto(new AutoConverter().from(source.getToAuto()));
			partecipazione.setGara(new GaraConverter().from(source.getToGara()));
			partecipazione.setPilota(new PilotaConverter().from(source.getToPilota()));
		}
		if (lev >= CONV_TYPE.NORMAL) {
			partecipazione.setVittoria(source.getVittoria());
			partecipazione.setGiroVeloce(source.getGiroVeloce());
		}
		if (lev >= CONV_TYPE.CASCADE) {
		}
		return partecipazione;
	}

	@Override
	public PartecipazioneDB to(Partecipazione source, int lev) {
		PartecipazioneDB partecipazione = new PartecipazioneDB();
		if (lev >= CONV_TYPE.MINIMUM) {
			partecipazione.setToAuto(new AutoConverter().to(source.getAuto()));
			partecipazione.setToGara(new GaraConverter().to(source.getGara()));
			partecipazione.setToPilota(new PilotaConverter().to(source.getPilota()));
		}
		if (lev >= CONV_TYPE.NORMAL) {
			partecipazione.setGiroVeloce(source.isGiroVeloce());
			partecipazione.setVittoria(source.isVittoria());
		}
		if (lev >= CONV_TYPE.CASCADE) {
		}
		return partecipazione;
	}

}
