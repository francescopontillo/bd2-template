package net.frapontillo.uni.bd2.template.converter;

import net.frapontillo.uni.bd2.template.db.AutoDB;
import net.frapontillo.uni.bd2.template.entity.Auto;
import net.frapontillo.uni.bd2.template.util.DBUtil;

public class AutoConverter extends AbstractConverter<AutoDB, Auto> {
	@Override
	public Auto from(AutoDB source, int lev) {
		Auto auto = new Auto();
		if (lev >= CONV_TYPE.MINIMUM) {
			auto.setSigla((String) DBUtil.getID(source, AutoDB.SIGLA_PK_COLUMN));
			auto.setNome(source.getNome());
		}
		if (lev >= CONV_TYPE.NORMAL) {
			auto.setDescrizioneTecnica(source.getDescrizioneTecnica());
			auto.setPotenza(source.getPotenza());
			auto.setTipoMotore(source.getTipoMotore());
		}
		if (lev >= CONV_TYPE.CASCADE) {
			auto.setCollaudatori(
				new CollaudatoreConverter().fromList(
						source.getCollaudatoreArray()));
		}
		return auto;
	}

	@Override
	public AutoDB to(Auto source, int lev) {
		AutoDB auto = new AutoDB();
		if (lev >= CONV_TYPE.MINIMUM) {
			auto.setNome(source.getNome());
		}
		if (lev >= CONV_TYPE.NORMAL) {
			auto.setDescrizioneTecnica(source.getDescrizioneTecnica());
			auto.setPotenza(source.getPotenza());
			auto.setTipoMotore(source.getTipoMotore());
		}
		if (lev >= CONV_TYPE.CASCADE) {
		}
		return auto;
	}
}
