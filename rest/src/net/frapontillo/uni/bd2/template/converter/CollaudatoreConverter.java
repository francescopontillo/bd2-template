package net.frapontillo.uni.bd2.template.converter;

import net.frapontillo.uni.bd2.template.db.CollaudatoreDB;
import net.frapontillo.uni.bd2.template.entity.Collaudatore;

public class CollaudatoreConverter extends AbstractConverter<CollaudatoreDB, Collaudatore> {
	@Override
	public Collaudatore from(CollaudatoreDB source, int lev) {
		Collaudatore coll = (Collaudatore) new MembroConverter().from(source, lev);
		if (lev >= CONV_TYPE.MINIMUM) {
		}
		if (lev >= CONV_TYPE.NORMAL) {
			coll.setAutoCollaudo(new AutoConverter().from(source.getToAuto()));
		}
		if (lev >= CONV_TYPE.CASCADE) {
		}
		return coll;
	}

	@Override
	public CollaudatoreDB to(Collaudatore source, int lev) {
		CollaudatoreDB coll = (CollaudatoreDB) new MembroConverter().to(source, lev);
		if (lev >= CONV_TYPE.MINIMUM) {
		}
		if (lev >= CONV_TYPE.NORMAL) {
			coll.setToAuto(new AutoConverter().to(source.getAutoCollaudo()));
		}
		if (lev >= CONV_TYPE.CASCADE) {
		}
		return coll;
	}
}
