package net.frapontillo.uni.bd2.template.converter;

import net.frapontillo.uni.bd2.template.db.IngegnereDB;
import net.frapontillo.uni.bd2.template.entity.Ingegnere;

public class IngegnereConverter extends AbstractConverter<IngegnereDB, Ingegnere> {
	@Override
	public Ingegnere from(IngegnereDB source, int lev) {
		if (source == null) return null;
		Ingegnere ingegnere = (Ingegnere) new MembroConverter().from(source, lev);
		if (lev >= CONV_TYPE.MINIMUM) {
		}
		if (lev >= CONV_TYPE.NORMAL) {
			ingegnere.setSpecializzazione(source.getSpecializzazione());
		}
		if (lev >= CONV_TYPE.CASCADE) {
		}
		return ingegnere;
	}
	
	@Override
	public IngegnereDB to(Ingegnere source, int lev) {
		IngegnereDB ingegnere = (IngegnereDB) new MembroConverter().to(source, lev);
		if (lev >= CONV_TYPE.MINIMUM) {
		}
		if (lev >= CONV_TYPE.NORMAL) {
			ingegnere.setSpecializzazione(source.getSpecializzazione());
		}
		if (lev >= CONV_TYPE.CASCADE) {
		}
		return ingegnere;
	}
}
