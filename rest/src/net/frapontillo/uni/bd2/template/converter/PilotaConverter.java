package net.frapontillo.uni.bd2.template.converter;

import net.frapontillo.uni.bd2.template.db.PilotaDB;
import net.frapontillo.uni.bd2.template.entity.Pilota;

public class PilotaConverter extends AbstractConverter<PilotaDB, Pilota> {

	@Override
	public Pilota from(PilotaDB source, int lev) {
		if (source == null) return null;
		Pilota pilota = (Pilota) new MembroConverter().from(source, lev);
		if (lev >= CONV_TYPE.MINIMUM) {
		}
		if (lev >= CONV_TYPE.NORMAL) {
			pilota.setCampionati(source.getCampionati());
			pilota.setVittorie(source.getVittorie());
		}
		if (lev >= CONV_TYPE.CASCADE) {
		}
		return pilota;
	}

	@Override
	public PilotaDB to(Pilota source, int lev) {
		PilotaDB pilota = (PilotaDB) new MembroConverter().to(source, lev);
		if (lev >= CONV_TYPE.MINIMUM) {
		}
		if (lev >= CONV_TYPE.NORMAL) {
			pilota.setCampionati(source.getCampionati());
			pilota.setVittorie(source.getVittorie());
		}
		if (lev >= CONV_TYPE.CASCADE) {
		}
		return pilota;
	}

}
