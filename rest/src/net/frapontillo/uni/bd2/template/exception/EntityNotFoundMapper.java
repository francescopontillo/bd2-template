package net.frapontillo.uni.bd2.template.exception;

import java.util.NoSuchElementException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class EntityNotFoundMapper implements ExceptionMapper<NoSuchElementException> {
	@Override
	public Response toResponse(NoSuchElementException ex) {
		return Response.
				status(Status.NOT_FOUND).
				type(MediaType.TEXT_PLAIN).
				entity(ex.getMessage()).
				build();
	}
}
