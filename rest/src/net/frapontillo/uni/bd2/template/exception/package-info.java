/**
 * Package contenente le eccezioni ed i relativi mapping.
 * @author Francesco Pontillo
 *
 */
package net.frapontillo.uni.bd2.template.exception;