package net.frapontillo.uni.bd2.template.exception;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class NotAuthorizedMapper implements ExceptionMapper<NotAuthorizedException> {
	@Override
	public Response toResponse(NotAuthorizedException ex) {
		return Response.
				status(Status.UNAUTHORIZED).
				type(MediaType.TEXT_PLAIN).
				entity(ex.getMessage()).
				build();
	}
}
