package net.frapontillo.uni.bd2.template.db;

import net.frapontillo.uni.bd2.template.db.auto._BD2Map;

public class BD2Map extends _BD2Map {

    private static BD2Map instance;

    private BD2Map() {}

    public static BD2Map getInstance() {
        if(instance == null) {
            instance = new BD2Map();
        }

        return instance;
    }
}
