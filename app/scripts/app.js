'use strict';

var bd2TemplateApp = angular.module('bd2TemplateApp', ['ngResource']);

bd2TemplateApp.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl:'views/main.html',
            controller:'MainCtrl'
        })
        .when('/pilota', {
          templateUrl: 'views/pilota_list.html',
          controller: 'PilotaListCtrl'
        })
        .when('/pilota/:id', {
          templateUrl: 'views/pilota.html',
          controller: 'PilotaCtrl'
        })
        .when('/pilota/new', {
            templateUrl: 'views/pilota_new.html',
            controller: 'PilotaNewCtrl'
        })
        .when('/collaudatore', {
          templateUrl: 'views/collaudatore_list.html',
          controller: 'CollaudatoreListCtrl'
        })
        .when('/collaudatore/:id', {
          templateUrl: 'views/collaudatore.html',
          controller: 'CollaudatoreCtrl'
        })
        .when('/collaudatore/new', {
            templateUrl: 'views/collaudatore_new.html',
            controller: 'CollaudatoreNewCtrl'
        })
        .when('/ingegnere', {
          templateUrl: 'views/ingegnere_list.html',
          controller: 'IngegnereListCtrl'
        })
        .when('/ingegnere/:id', {
          templateUrl: 'views/ingegnere.html',
          controller: 'IngegnereCtrl'
        })
        .when('/ingegnere/new', {
            templateUrl: 'views/ingegnere_new.html',
            controller: 'IngegnereNewCtrl'
        })
        .when('/gara', {
          templateUrl: 'views/gara_list.html',
          controller: 'GaraListCtrl'
        })
        .when('/gara/:id', {
          templateUrl: 'views/gara.html',
          controller: 'GaraCtrl'
        })
        .when('/gara/new', {
            templateUrl: 'views/gara_new.html',
            controller: 'GaraNewCtrl'
        })
        .when('/auto', {
          templateUrl: 'views/auto_list.html',
          controller: 'AutoListCtrl'
        })
        .when('/auto/:id', {
          templateUrl: 'views/auto.html',
          controller: 'AutoCtrl'
        })
        .when('/auto/new', {
          templateUrl: 'views/auto_new.html',
          controller: 'AutoNewCtrl'
        })
        .otherwise({
            redirectTo:'/'
        });
}]);

bd2TemplateApp.run(function ($rootScope, $location, MenuItem) {
    // Radice del menu
    $rootScope.menu = new MenuItem({
        name: "F1",
        url: function() { return ["/"]; }
    });

    // Elementi singoli del menu : primo livello
    $rootScope.mPiloti = new MenuItem({
        name: "Piloti",
        url: function() { return ["/pilota"]; },
        menuOrder: 0
    });
    $rootScope.mCollaudatori = new MenuItem({
        name: "Collaudatori",
        url: function() { return ["/collaudatore"]; },
        menuOrder: 1
    });
    $rootScope.mIngegneri = new MenuItem({
        name: "Ingegneri",
        url: function() { return ["/ingegnere"]; },
        menuOrder: 2
    });
    $rootScope.mGare = new MenuItem({
        name: "Gare",
        url: function() { return ["/gara"]; },
        menuOrder: 3
    });
    $rootScope.mAuto = new MenuItem({
        name: "Auto",
        url: function() { return ["/auto"]; },
        menuOrder: 4
    });

    // Creo la gerarchia del menu
    $rootScope.menu.addChildren([
        $rootScope.mPiloti,
        $rootScope.mIngegneri,
        $rootScope.mCollaudatori,
        $rootScope.mGare,
        $rootScope.mAuto
    ]);

    $rootScope.curUrl = function() {
        return $location.path();
    };
});