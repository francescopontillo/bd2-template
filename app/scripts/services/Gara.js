'use strict';

bd2TemplateApp.factory('Gara', function($resource, Config) {
	var GaraResource = $resource(Config.baseAPI() + 'gara/:id');

	GaraResource.prototype.fill = function() {
		fill(this);
	};

	GaraResource.prototype.fillAll = function(objects) {
		if (!objects) return;
		for (var obj in objects) {
			fill(objects[obj]);
		}
	};

	var fill = function(obj) {
	};

	return GaraResource;
});
