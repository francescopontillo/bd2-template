'use strict';

bd2TemplateApp.factory('Config', function() {
  var baseAPI = "http://localhost\\:8080/Bd2RestTemplate/api/";

  // Public API here
  return {
    baseAPI: function() {
      return baseAPI;
    }
  };
});
