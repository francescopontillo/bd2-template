'use strict';

describe('Controller: Collaudatore/NewCtrl', function() {

  // load the controller's module
  beforeEach(module('bd2TemplateApp'));

  var Collaudatore/NewCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($controller) {
    scope = {};
    Collaudatore/NewCtrl = $controller('Collaudatore/NewCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function() {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
