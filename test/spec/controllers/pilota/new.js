'use strict';

describe('Controller: Pilota/NewCtrl', function() {

  // load the controller's module
  beforeEach(module('bd2TemplateApp'));

  var Pilota/NewCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($controller) {
    scope = {};
    Pilota/NewCtrl = $controller('Pilota/NewCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function() {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
