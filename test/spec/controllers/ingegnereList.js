'use strict';

describe('Controller: IngegnereListCtrl', function() {

  // load the controller's module
  beforeEach(module('bd2TemplateApp'));

  var IngegnereListCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($controller) {
    scope = {};
    IngegnereListCtrl = $controller('IngegnereListCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function() {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
