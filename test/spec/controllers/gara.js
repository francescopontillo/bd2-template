'use strict';

describe('Controller: GaraCtrl', function() {

  // load the controller's module
  beforeEach(module('bd2TemplateApp'));

  var GaraCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($controller) {
    scope = {};
    GaraCtrl = $controller('GaraCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function() {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
