'use strict';

describe('Controller: AutoCtrl', function() {

  // load the controller's module
  beforeEach(module('bd2TemplateApp'));

  var AutoCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($controller) {
    scope = {};
    AutoCtrl = $controller('AutoCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function() {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
