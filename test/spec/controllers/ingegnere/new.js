'use strict';

describe('Controller: Ingegnere/NewCtrl', function() {

  // load the controller's module
  beforeEach(module('bd2TemplateApp'));

  var Ingegnere/NewCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($controller) {
    scope = {};
    Ingegnere/NewCtrl = $controller('Ingegnere/NewCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function() {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
