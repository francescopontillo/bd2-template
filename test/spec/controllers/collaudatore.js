'use strict';

describe('Controller: CollaudatoreCtrl', function() {

  // load the controller's module
  beforeEach(module('bd2TemplateApp'));

  var CollaudatoreCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($controller) {
    scope = {};
    CollaudatoreCtrl = $controller('CollaudatoreCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function() {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
