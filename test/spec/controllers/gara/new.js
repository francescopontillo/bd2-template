'use strict';

describe('Controller: Gara/NewCtrl', function() {

  // load the controller's module
  beforeEach(module('bd2TemplateApp'));

  var Gara/NewCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($controller) {
    scope = {};
    Gara/NewCtrl = $controller('Gara/NewCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function() {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
