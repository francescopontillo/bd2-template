'use strict';

describe('Controller: IngegnereCtrl', function() {

  // load the controller's module
  beforeEach(module('bd2TemplateApp'));

  var IngegnereCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($controller) {
    scope = {};
    IngegnereCtrl = $controller('IngegnereCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function() {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
