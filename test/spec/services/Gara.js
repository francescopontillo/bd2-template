'use strict';

describe('Service: Gara', function () {

  // load the service's module
  beforeEach(module('bd2TemplateApp'));

  // instantiate service
  var Gara;
  beforeEach(inject(function(_Gara_) {
    Gara = _Gara_;
  }));

  it('should do something', function () {
    expect(!!Gara).toBe(true);
  });

});
