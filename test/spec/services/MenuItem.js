'use strict';

describe('Service: MenuItem', function () {

  // load the service's module
  beforeEach(module('bd2TemplateApp'));

  // instantiate service
  var MenuItem;
  beforeEach(inject(function(_MenuItem_) {
    MenuItem = _MenuItem_;
  }));

  it('should do something', function () {
    expect(!!MenuItem).toBe(true);
  });

});
